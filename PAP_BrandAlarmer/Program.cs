﻿using System;

namespace PAP_BrandAlarmer
{
    enum AlarmType
    {
        Geen,
        Test,
        Echt
    }

    class Program
    {
        static void Main(string[] args)
        {
            double[] jaarLogboek = new double[10];
            int jaarCounter = 0;
            bool shouldRun = true;
            do
            {
                AlarmType[] weekLogboek = new AlarmType[52];
                //weekloop
                Random r = new Random();
                for (int i = 0; i < 52; i++)
                {
                    switch (i)
                    {
                        case 1:
                        case 11:
                        case 21:
                        case 31:
                        case 41:
                        case 51:
                            TestAlarm();
                            weekLogboek[i] = (AlarmType.Test);
                            break;
                        default:
                            if (r.Next(1, 5) == 1)
                            {
                                int gebouwId = r.Next(1, 4);
                                StartAlarmGebouw(gebouwId, false);
                                weekLogboek[i] = AlarmType.Echt;
                            }
                            break;
                    }
                }
                ToonLogboek(weekLogboek);
                jaarLogboek[jaarCounter] = BerekenSommen(weekLogboek);
                jaarCounter++;

                //continue?
                if (jaarCounter < 10)
                {
                    Console.Write("Moet er nog een jaar gestart worden? (j/n): ");
                    string tmp = Console.ReadLine().ToLower();
                    if (tmp.StartsWith('n'))
                        shouldRun = false;
                }
            } while (shouldRun && jaarCounter < 10);
            Eindverslag(jaarLogboek);
        }

        private static void Eindverslag(double[] jaarLogboek)
        {
            Console.WriteLine("Verslag:");
            for (int i = 0; i < jaarLogboek.Length; i++)
            {
                if (jaarLogboek[i] == 0.0)
                    continue;
                if (jaarLogboek[i] < 10.0)
                    Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("Jaar {0}: {1:F2}%", i + 1, jaarLogboek[i]);
            }
        }

        private static double BerekenSommen(AlarmType[] weekLogboek)
        {
            int numEchteAlarmen = 0;
            for (int i = 0; i < weekLogboek.Length; i++)
            {
                if (weekLogboek[i] == AlarmType.Echt)
                    numEchteAlarmen++;
            }
            double gemiddelde = Convert.ToDouble(numEchteAlarmen) / 52.0 * 100.0;
            Console.WriteLine($"Gemiddelde echte alarmen = {gemiddelde:f2}%");

            return gemiddelde;
        }

        private static void ToonLogboek(AlarmType[] logboek)
        {
            int[] range = { 1, 2, 3, 4, 5, 6, 7, 8, 9 };
            int rangeCounter = 0;
            for (int i = 0; i < logboek.Length; i++)
            {
                if (logboek[i] == AlarmType.Echt)
                {
                    Console.BackgroundColor = ConsoleColor.Red;
                }
                else if (logboek[i] == AlarmType.Test)
                {
                    Console.BackgroundColor = ConsoleColor.Green;
                }
                else
                {
                    Console.BackgroundColor = ConsoleColor.Blue;
                }
                if (rangeCounter >= range.Length - 1)
                {
                    rangeCounter = 0;
                }
                Console.Write(range[rangeCounter]);
                rangeCounter++;
                Console.ResetColor();
            }
            Console.WriteLine();
        }

        private static void TestAlarm()
        {
            int r = new Random().Next(1, 101);
            int gebouwId;
            if (r <= 20)
            {
                gebouwId = 1;
            }
            else if (r <= 50)
            {
                gebouwId = 2;
            }
            else
            {
                gebouwId = 3;
            }
            StartAlarmGebouw(gebouwId, true);
        }

        private static void StartAlarmGebouw(int gebouwId, bool isTestAlarm)
        {
            string tmp = isTestAlarm ? " TEST." : " ECHT alarm.";
            Console.WriteLine("Gebouw {0} geëvacueerd tgv alarm - Dit was een {1}",
              gebouwId, tmp);
        }
    }
}